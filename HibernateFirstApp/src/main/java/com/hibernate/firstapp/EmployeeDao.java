package com.hibernate.firstapp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

public class EmployeeDao {
	public void saveEmployee(Employee employee) {
		Transaction transaction = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			//Start transaction
			transaction = session.beginTransaction();
			
			//save employee object
			session.save(employee);
			
			//commit transaction
			transaction.commit();
		}catch(Exception ex) {
			if(transaction != null) {
			transaction.rollback();
			}
			ex.printStackTrace();
	}
	}
	
	public List<Employee> getEmployeeList(){
		Transaction transaction = null;
		List<Employee> empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			empList = session.createQuery("from Employee").list();
//			ransaction.commit();
		}catch(Exception ex) {
			if(transaction != null) {
				transaction.rollback();
				}
				ex.printStackTrace();
		}
		return empList;
	}
	
	public void UpdateEmployeeName(int id,String name) {
		Transaction transaction = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			Employee emp = session.get(Employee.class, id);
			emp.setName(name);
			//session.update(emp);
			
			transaction.commit();
			session.close();
		}catch(Exception ex) {
			if(transaction != null) {
				transaction.rollback();
				}
				ex.printStackTrace();
		}
		
	}
	
	public void deleteEmployee(int id) {
		Transaction transaction = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			transaction = session.beginTransaction();
			Employee emp = session.get(Employee.class, id);
			session.delete(emp);
			transaction.commit();
		}catch(Exception ex) {
			if(transaction != null) {
				transaction.rollback();
				}
				ex.printStackTrace();
		}
	}
	
	public void UpdateEmployee(int id,String name,String city) {
		Transaction transaction = null;
		try{
			Session session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			Employee emp = session.get(Employee.class, id);
			emp.setName(name);
			session.update(emp);
			
			//transaction.commit();
			session.close();
			
			emp.setCity(city);
			Session session2 = HibernateUtil.getSessionFactory().openSession();
			transaction = session2.beginTransaction();
			session2.update(emp);
			transaction.commit();
			session2.close();
		}catch(Exception ex)
		{
			System.out.print(ex);
			if(transaction != null) {
				transaction.rollback();
				}
				ex.printStackTrace();
		}
		
	}
}
