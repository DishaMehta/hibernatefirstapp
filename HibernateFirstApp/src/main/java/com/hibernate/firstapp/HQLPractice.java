package com.hibernate.firstapp;

import java.util.List;

import org.hibernate.query.Query;
import org.hibernate.Session;

public class HQLPractice {
	
	public List getEmployeeList(){
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			Query query = session.createQuery("from Employee");
			empList = query.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;   
	}
	
	public List getEmployeeNames(){
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			String str = "Select e.Name,e.City from Employee As e";
			Query query = session.createQuery(str);
			empList = query.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;   
	}
	
	public List getEmployeeGroupBySalary() {
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			String str = "Select e.Name,SUM(e.Salary) from Employee As e group by e.Name order by e.Name";
			Query query = session.createQuery(str);
			empList = query.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;  
	}
	
	public void updateName(String name,int sal) {
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			session.beginTransaction();
			String str = "Update Employee set Name = :NAME where Salary = :SAL";
			Query query = session.createQuery(str);
			query.setParameter("NAME", name);
			query.setParameter("SAL", sal);
			int res = query.executeUpdate();
			session.getTransaction().commit();
		}catch(Exception ex) {
				ex.printStackTrace();
				System.out.println(ex);
		}
		  
	}
}
