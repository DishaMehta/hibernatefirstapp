package com.hibernate.firstapp;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;

public class CriteriaPractice {
	public List getEmployeeList(){
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			Criteria cr = session.createCriteria(Employee.class);
			cr.addOrder(Order.asc("Salary"));
			empList = cr.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;   
	}
	
	public List getEmployeeByCriteria(int sal) {
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			Criteria cr = session.createCriteria(Employee.class);
			cr.add(Restrictions.gt("Salary",sal));
			cr.addOrder(Order.asc("Name"));
			empList = cr.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;
	}
	
	public List getEmployeeByCriteriANDOR(int sal,String nm) {
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			Criteria cr = session.createCriteria(Employee.class);
			LogicalExpression orExp = Restrictions.or(Restrictions.gt("Salary",sal),Restrictions.ilike("Name", nm));
			//LogicalExpression AndExp = Restrictions.and(Restrictions.gt("Salary",sal),Restrictions.ilike("Name", nm));
			cr.add(orExp);
			
			//For pagination
			//cr.setFirstResult(1);
			//cr.setMaxResults(2);
			empList = cr.list();
		}catch(Exception ex) {
				ex.printStackTrace();
		}
		return empList;
	}
	
	public void getEmployeeCount() {
		List empList = null;
		try(Session session = HibernateUtil.getSessionFactory().openSession()){
			 Criteria cr = session.createCriteria(Employee.class);
	         //cr.setProjection(Projections.rowCount());
			 //cr.setProjection(Projections.max("Salary"));
			 //cr.setProjection(Projections.sum("Salary"));
			 cr.setProjection(Projections.avg("Salary"));
	         empList = cr.list();
	         //System.out.println("Total Employee Count:" + empList.get(0));
	         System.out.println(empList.get(0));
		}catch(Exception ex) {
				ex.printStackTrace();
		}
	}
}
