package com.hibernate.firstapp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Employee",schema = "person")
public class Employee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="Id")
	private int Id;
	
	@Column(name="Name")
	private String Name;
	
	@Column(name="Address")
	private String Address;
	
	@Column(name="City")
	private String City;
	
	@Column(name="Salary")
	private int Salary;
	
	public Employee() {
		super();
	}

	public Employee(String Name, String Address, String City, int Salary) {
		this.Name = Name;
		this.Address = Address;
		this.City = City;
		this.Salary = Salary;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		this.Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getAddress() {
		return Address;
	}
 
	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public int getSalary() {
		return Salary;
	}

	public void setSalary(int salary) {
		this.Salary = salary;
	}
	
	
	
	
}
