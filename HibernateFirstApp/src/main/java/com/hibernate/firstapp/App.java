package com.hibernate.firstapp;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;

public class App {
	public static void main(String[] args) {
		
		EmployeeDao employeeDao = new EmployeeDao();
		
		//Insert
		Employee employee = new Employee("Disha","Pal","Surat",35000);
		employeeDao.saveEmployee(employee);
		
		employee = new Employee("Anisha","Adajan","Surat",20000);
		employeeDao.saveEmployee(employee);
		
		employee = new Employee("Anisha","Adajan","Surat",40000);
		employeeDao.saveEmployee(employee);
		
		//List
//		List<Employee> emplist = employeeDao.getEmployeeList();
//		Iterator itr = emplist.iterator();
//		while(itr.hasNext()) {
//			Employee emp = (Employee)itr.next();
//			System.out.println(emp.getName() + " " + emp.getAddress() + " " +emp.getCity() + " "+emp.getSalary());
//		}
		
		//Update
		//employeeDao.UpdateEmployeeName(1, "DISHA");
		
		//update Extra
		//employeeDao.UpdateEmployee(1,"DIs","Ss");
		
		//Delete
		//employeeDao.deleteEmployee(1);
		
		//get all
		HQLPractice hp = new HQLPractice();
		List<Employee> emplist = hp.getEmployeeList();
		Iterator itr = emplist.iterator();
		while(itr.hasNext()) {
			Employee emp = (Employee)itr.next();
			System.out.println(emp.getName()+ " " + emp.getAddress() + " " +emp.getCity() + " "+emp.getSalary());
		}
		
		//get name + city
		emplist = hp.getEmployeeNames();
		itr = emplist.iterator();
		while(itr.hasNext()) {
			Object[] emp = (Object[])itr.next();
			System.out.println(emp[0] + " " +emp[1]);
		}
		
		System.out.println("=========================");
		
		//group by salary
		emplist = hp.getEmployeeGroupBySalary();
		itr = emplist.iterator();
		while(itr.hasNext()) {
			Object[] emp = (Object[])itr.next();
			System.out.println(emp[0] + " " +emp[1]);
		}
		
		System.out.println("=========================");
		
		//Update
		hp.updateName("Disha", 40000);
		
		//Criteria
		System.out.println("=========================");
		System.out.println("CRITERIA START");
		System.out.println("=========================");
		
		CriteriaPractice c = new CriteriaPractice();
		emplist = c.getEmployeeList();
		itr = emplist.iterator();
		while(itr.hasNext()) {
			Employee emp = (Employee)itr.next();
			System.out.println(emp.getName()+ " " + emp.getAddress() + " " +emp.getCity() + " "+emp.getSalary());
		}
		
		System.out.println("=========================");
		emplist = c.getEmployeeByCriteria(20000);
		itr = emplist.iterator();
		while(itr.hasNext()) {
			Employee emp = (Employee)itr.next();
			System.out.println(emp.getName()+ " " + emp.getAddress() + " " +emp.getCity() + " "+emp.getSalary());
		}
		
		System.out.println("=========================");
		emplist = c.getEmployeeByCriteriANDOR(10000, "Di%");
		itr = emplist.iterator();
		while(itr.hasNext()) {
			Employee emp = (Employee)itr.next();
			System.out.println(emp.getName()+ " " + emp.getAddress() + " " +emp.getCity() + " "+emp.getSalary());
		}
		
		System.out.println("=========================");
c.getEmployeeCount();
	}
}
